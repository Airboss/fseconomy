<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="java.util.List, net.fseconomy.beans.*, net.fseconomy.data.*, net.fseconomy.util.*"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session"/>

<%
    if (!user.isLoggedIn())
    {
%>
<script type="text/javascript">document.location.href = "/index.jsp"</script>
<%
        return;
    }

    //setup return page if action used
    String returnPage = "airportexpenses.jsp"; //request.getHeader("referer");

    String icao = request.getParameter("icao");
    if(Helpers.isNullOrBlank(icao))
    {
        icao = Helpers.getSessionIcao(request);
        if(Helpers.isNullOrBlank(icao))
        {
            //just return a default if unable to find an icao
            icao = "KSTL";
        }
    }

    List<FboBean> fbos = Fbos.getFboByLocation(icao);
    CachedAirportBean airport = Airports.cachedAirports.get(icao);
%>
<jsp:include flush="true" page="/head.jsp" />
    <script src="scripts/autocomplete-icao.js"></script>
    <script>
        $(function()
        {
            initAutoCompleteIcao("#icao-entry", "#to-icao");
        });

        $(function() {
            $("#get-ticket-quote").click(function () {
                var fromIcao = $("#from-icao").val();
                var toIcao = $("#to-icao").val();

                console.log("fromIcao", fromIcao);
                console.log("entry-icao", $("#icao-entry").val());
                console.log("toIcao", toIcao);

                if (toIcao === undefined || toIcao === "") {
                    window.alert("Please enter a destination ICAO!");
                    $("#icao-entry").focus();
                    return;
                }

                $.ajax({
                    type: "GET",
                    url: "/rest/api/v2/ticketquote/" + fromIcao + '/' + toIcao,
                    dataType: "json",
                    success: function (response) {
                        $("#ticket-price").text(response.data);
                    },
                    error: function (e) {
                        alert('Error: ' + e);
                    }
                });
            });

            $("#button-charge").click(function () {
                if(window.confirm("Select [Ok] to approve these expense charges, [Cancel] to cancel."))
                {
                    var form = document.getElementById("form-charges");
                    form.submit();
                }
            });

            $("#button-charge2").click(function () {
                if(window.confirm("Select [Ok] to approve these expense charges, [Cancel] to cancel."))
                {
                    var form = document.getElementById("form-onetime");
                    form.submit();
                }
            });

            $("#button-purchase-ticket").click(function () {
                if(window.confirm("Select [Ok] to approve these ticket charges, [Cancel] to cancel."))
                {
                    var form = document.getElementById("form-ticket");
                    form.submit();
                }
            });

            $('#custom-exp').click(function(){
                if(this.checked == true) {
                    $("#custexpdiv").show();
                } else {
                    $("#custexpdiv").hide();
                }
            });
            $('#custom-exp-cost').change(function(){
                var v = $('#custom-exp-cost').val();

                if(v < 1) {
                    $('#custom-exp-cost').val(1);;
                }
                else if(v > <%=Expenses.CUSTOM_COST_MAX%>){
                    $('#custom-exp-cost').val(2500);
                }
            });
            $('#custom-exp-cost').focusout(function(){
                var v = $('#custom-exp-cost').val();

                if(v === undefined || v === '') {
                    $('#custom-exp-cost').val(1);
                }
            });
            $("#custom-exp-cost").keypress(function(e){
                var keyCode = e.which;
                /*
                 8 - (backspace)
                 48-57 - (0-9)Numbers
                 */

                if ( (keyCode != 8) && (keyCode < 48 || keyCode > 57)) {
                    return false;
                }
            });
        });
    </script>
    <style>
        input {
            padding: 3px;
        }
    </style>
</head>
<body>

<jsp:include flush="true" page="top.jsp"/>
<jsp:include flush="true" page="menu.jsp"/>

<div id="wrapper">
    <div class="content">
<%
    String message = Helpers.getSessionMessage(request);
    if (message != null)
    {
%>
<div class="error"><%= message %></div>
<%
    }
%>

        <div class="dataTable">
            <h4><a href="airport.jsp?icao=<%=icao%>">Return to <%=icao.toUpperCase()%></a></h4>
            <div class="content">
                <form method="post" id="form-charges" action="userctl">
                    <div>
                        <input type="hidden" name="event" value="expenses"/>
                        <input type="hidden" name="id" value="<%= user.getId() %>"/>
                        <input type="hidden" name="icao" value="<%= icao %>"/>
                        <input type="hidden" name="returnpage" value="<%=returnPage%>"/>
                    </div>
                    <table>
                        <caption>Local Accommodations / Services</caption>
                        <tbody>
                        <tr>
                            <td>
                                <input name="hotel" id="hotel" type="checkbox">
                                <%=Formatters.currency.format(Expenses.HOTEL_COST)%> (per day)
                            </td>
                            <td>Hotel</td>
                        </tr>
                        <tr>
                            <td>
                                <input name="rental-car" id="rental-car" type="checkbox">
                                <%=Formatters.currency.format(Expenses.RENTALCAR_COST)%> (per day)
                            </td>
                            <td>Car Rental</td>
                        </tr>
                        <tr>
                            <td>
                                <input name="meals" id="meals" type="checkbox">
                                <%=Formatters.currency.format(Expenses.MEALS_COST)%> (per day)
                            </td>
                            <td>Meals & Snacks</td>
                        </tr>
                        <tr>
                            <td>
                                <input name="tiedown" id="tiedown" type="checkbox">
                                <%=Formatters.currency.format(Expenses.TIEDOWN_COST)%> (per day)
                            </td>
                            <td>Aircraft Tiedown</td>
                        </tr>
                        <tr style="font-size: larger;">
                            <td>
                                <input name="total-days" id="total-days" type="number" min="0" max="30" value="1">
                            </td>
                            <td>Days Requested</td>
                        </tr>
                        </tbody>
                    </table>
                    <div>
                        Buy from
                        <select name="provider" class="formselect">
                            <%
                                for (int c = 0; c < fbos.size(); c++)
                                {
                            %>
                            <option class="formselect" value="<%= fbos.get(c).getId() %>"><%= fbos.get(c).getName() %>
                            </option>
                            <%
                                }
                            %>
                            <option class="formselect" value="0">Local market</option>
                            <option class="formselect" value="" selected="selected">Choose Provider</option>
                        </select>
                        <input type="button" id="button-charge" class="button" value="Apply Charges"/>
                    </div>
                </form>

                <form method="post" id="form-onetime" action="userctl">
                    <div>
                        <input type="hidden" name="event" value="onetime-expenses"/>
                        <input type="hidden" name="id" value="<%= user.getId() %>"/>
                        <input type="hidden" name="icao" value="<%= icao %>"/>
                        <input type="hidden" name="returnpage" value="<%=returnPage%>"/>
                    </div>
                    <table>
                        <caption>Purchase</caption>
                        <tbody>
                        <tr>
                            <td>
                                <input name="qts-oil" id="qts-oil" type="number" min="1" max="24" value="0" size="2">
                                <%=Formatters.currency.format(Expenses.QT_OIL_COST)%> (each)
                            </td>
                            <td>Quarts of Oil</td>
                        </tr>
                        <tr>
                            <td>
                                <input name="o2-refill" id="o2-refill" type="checkbox">
                                <%=Formatters.currency.format(Expenses.O2_REFILL_COST)%>
                            </td>
                            <td>O2 Refill</td>
                        </tr>
                        <tr>
                            <td>
                                <input name="wash" id="wash" type="checkbox">
                                <%=Formatters.currency.format(Expenses.WASH_COST)%>
                            </td>
                            <td>Plane Wash</td>
                        </tr>
                        <tr>
                            <td>
                                <input name="custom-exp" id="custom-exp" type="checkbox">
                                Custom
                            </td>
                            <td>
                                <div id="custexpdiv" style="display:none;">
                                Cost:
                                <input name="custom-exp-cost" id="custom-exp-cost" type="number" min="1" max="2500" value="1" size="4">($2500 max)<br>
                                Desc*:
                                <input name="custom-exp-desc" id="custom-exp-desc" type="text" size="25" maxlength="25"> (25 max)<br>
                                (*TOS rules apply, keep it aviation related)
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div>
                        Buy from
                        <select name="provider" class="formselect">
                            <%
                                for (int c = 0; c < fbos.size(); c++)
                                {
                            %>
                            <option class="formselect" value="<%= fbos.get(c).getId() %>"><%= fbos.get(c).getName() %>
                            </option>
                            <%
                                }
                            %>
                            <option class="formselect" value="0">Local market</option>
                            <option class="formselect" value="" selected="selected">Choose Provider</option>
                        </select>
                        <input type="button" id="button-charge2" class="button" value="Apply Charges"/>
                    </div>
                </form>

                <form class="form" method="post" action="userctl" id="form-ticket">
                    <div>
                        <input type="hidden" name="event" value="transport-ticket"/>
                        <input type="hidden" name="id" value="<%= user.getId() %>"/>
                        <input type="hidden" id="from-icao" name="from-icao" value="<%= icao %>"/>
                        <input type="hidden" name="returnpage" value="<%=returnPage%>"/>
                    </div>
                    <table>
                        <caption>Purchase Travel Ticket</caption>
                        <tbody>
                        <tr>
                            <td colspan="2">Transport funds go to FSE Airlines (Bank)</td>
                        </tr>
                        <tr>
                            <td>
                                From: <%= icao %>
                            </td>
                            <td>

                                <div id="byAutoComplete">
                                    To: <input type="text" id="icao-entry" name="icao-entry"  size="4"/>
                                    <input type="hidden" id="to-icao" name="to-icao" value=""/> (auto-complete)
                                </div>
                            </td>
                            <td colspan="2">
                                <div class="formgroup">
                                    <input type="button" id="button-purchase-ticket" class="button pull-right" value="Purchase Ticket" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div class="formgroup">
                                    Quote price for your transport ticket will be <span id="ticket-price">$0.00</span>
                                </div>
                            </td>
                            </tr>
                        <tr>
                            <td colspan="2">
                                <div class="formgroup">
                                    <input type="button" class="button pull-left" id="get-ticket-quote" value="Get Quote" />
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>

            </div>
        </div>
    </div>

</div>
</body>
</html>
